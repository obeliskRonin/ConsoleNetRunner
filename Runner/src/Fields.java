import java.util.ArrayList;
import java.util.Arrays;

public class Fields{

    private boolean leave;
    private Mode mode;
    private boolean gameStarted;

    public static final ArrayList<String> commands
        = new ArrayList<String>(Arrays.asList(new String[]
        {"q", "c", "d"}));

    public Fields(){

        leave = false;
        mode = new Mode();
        gameStarted = false;
    }
    
    private void setLeave(boolean newLeave){
        leave = newLeave;
    }
    
    public boolean getLeave(){
        return leave;
    }

    public void leave(){
        leave = true;
    }

    public Mode getMode(){
        return mode;
    }

    public boolean getGameStarted(){
        return gameStarted;
    }

    public void gameStarted(){
        gameStarted = true;
    }
}
