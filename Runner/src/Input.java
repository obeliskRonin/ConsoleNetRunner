import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Input implements Runnable{

    private Fields fields;
    private DataInputStream in;
    private DataOutputStream out;

    public Input(DataInputStream inIn, Fields fieldsIn, DataOutputStream outIn){

        in = inIn;
        fields = fieldsIn;
        out = outIn;
    }

    public void run(){

        while(true){

            try{
                String message = in.readUTF();
                String delete = "";
                for(int i = 0; i < fields.getMode().getModeText().length(); i++){
                    delete = delete + "\b";
                }
                System.out.println(delete);
//                System.out.print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
                System.out.println(Constants.normalText + message
                    + "                ");
                if(message.matches(Constants.quit)){
                    System.exit(0);
                }
                if(!fields.getLeave()){
                    System.out.print(fields.getMode().getModeText());
                }
            } catch(Exception e){
                System.out.println("Hey there is an exception in the class "
                    + "Input");
                System.out.println(e);
            }

            try{
                Thread.sleep(200);
            } catch(Exception e){
            }
        }
    }                                                           
}
