import java.io.DataOutputStream;
import java.io.IOException;

public class Commands{

    public Fields fields;
    public DataOutputStream out;
    
    public Commands(Fields fieldsIn, DataOutputStream outIn){

        fields = fieldsIn;
        out = outIn;
    }

    public void command(int number){

        if(number == 0){
            quit();
        }
        else if(number ==1){
            chat();
        }
    }
    
    private void quit(){
        fields.leave();
        try{
            out.writeUTF(Constants.farewell);
            out.writeUTF(Constants.quit);
        } catch(IOException e){
            System.out.println("There was an IOException when trying to send to"
                + "the Server. This iwas in the class Actions");
            System.out.println();
            System.out.println(e);
        }
        System.out.print("[0m");
        System.exit(0);
    }

    private void chat(){
        fields.getMode().setMode(Modes.CHAT);
    }
}
