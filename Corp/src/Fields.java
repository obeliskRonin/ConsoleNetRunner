import java.util.ArrayList;
import java.util.Arrays;

public class Fields{

    private boolean leave;
    private Mode mode;
    private boolean gameStarted;

    public static final ArrayList<String> commands
        = new ArrayList<String>(Arrays.asList(new String[]{"q", "c"}));

    public Fields(){

        leave = false;
        mode = new Mode();
        gameStarted = false;
    }

    public void leave(){
        leave = true;
    }

    public boolean getLeave(){
        return leave;
    }

    public Mode getMode(){
        return mode;
    }

    public boolean getGameStarted(){
        return gameStarted;
    }

    public void gameStarted(){
        gameStarted = true;
    }
}
