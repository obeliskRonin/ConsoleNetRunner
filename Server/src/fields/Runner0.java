package fields;

import java.util.ArrayList;

public class Runner0{

    private ArrayList<String> deck;
    private boolean makingDeck;

    public Runner0(){
        makingDeck = true;
        deck = new ArrayList<String>();
    }

    public String addCard(String card){

        if(makingDeck){
            deck.add(card);
            return Constants.normalText + "Successfully added card";
        }
        else{
            return Constants.normalText + "You said you were done making your "
                + "deck";
        }
    }

    public void doneMakindDeck(){
        makingDeck = false;
        shuffle();
    }

    private void shuffle(){
    }
}
