package fields;

public class Constants {

    public static final String normalText = (char)27 + "[0;40;37m";
    public static final String corp = "Corp";
    public static final String runner = "Runner";
    public static final String server = "Server";
    public static final String slash = "/";
    public static final String quit = "/q";
    public static final String q = "q";
    public static final String c = "c";
    public static final String serverInput = (char)27 + "[1;36m"
        + "Server> " + (char)27 + "[1;32m";
    public static final String serverChat = (char)27 + "[1;37m";
    public static final String actionPrompts = "[1;30m";
    public static final String farewell = normalText + "Server closed";
}
