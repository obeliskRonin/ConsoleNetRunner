package fields;

import java.io.*;
import java.net.Socket;
import java.net.URISyntaxException;
import staticpackage.Static;
import java.util.ArrayList;
import staticpackage.PrintLine;
import java.security.CodeSource;
import server.Launch;

public class Fields{

    public Threads threads;
    public WaitObject waitObject;
    private boolean connectCorp;
    private boolean connectRunner;
    private ArrayList<String> output;
    private String openError;
    private String fileRead;
    private Runner0 runner;
    private Corp0 corp;
    private String jarDir;
    
    public Fields(){
    
        try{
            CodeSource codeSource = Launch.class.getProtectionDomain()
                .getCodeSource();
            File jarFile = new File(codeSource.getLocation().toURI().getPath());
            jarDir = jarFile.getParentFile().getPath();
        } catch(URISyntaxException e){
            System.out.println("Error trying to get the path of the jar file.");
            System.out.println("Fields constructor");
            System.out.println(e);
        }
        System.out.println(jarDir);
        threads = new Threads();
        waitObject = new WaitObject();
        connectRunner = true;
        connectCorp = true;
        output = new ArrayList<String>();
        fileRead = null;
        runner = new Runner0();
        corp = new Corp0();
    }

    public String getJarDir(){
        return jarDir;
    }
//                                                                             |
    public void setConnectCorp(boolean connectCorpIn){

        connectCorp = connectCorpIn;
    }
//                                                                             |
    public boolean getConnectCorp(){
//                                                                             |
        return connectCorp;
    }
//                                                                             |

    public  void setConnectRunner(boolean connectRunnerIn){

        connectRunner = connectRunnerIn;
    }

    public boolean getConnectRunner(){

        return connectRunner;
    }

    public void newMessage(String message){

        output.add(message);
    }

    public ArrayList<String> getMessages(){

        return output;
    }
    
    public boolean save(){

        System.out.println(fileRead);
        
        if(fileRead == null){
            return false;
        }

        save(fileRead);
        System.out.println(fileRead);
        return true;
    }

    public boolean save(String fileName){

        FileWriter fileWriter;
        BufferedWriter bufferedWriter;
        try{
            fileWriter = new FileWriter(jarDir + "/saves/" + fileName);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch(IOException e){
            System.out.println(e);
            return false;
        }
//                                                                             |
        ArrayList<String> list = Static.copyArrayListString(output);
        int length = list.size();
        try{
            bufferedWriter.write(length + "");
            bufferedWriter.newLine();
        } catch(IOException e){
            System.out.println(e);
            return false;
        }
        for(int i = 0; i < length; i++){
            try{
                bufferedWriter.write("" + list.get(i));
                bufferedWriter.newLine();
            } catch(IOException e){
                System.out.println(e);
            }
            PrintLine.println("" + list.get(i));
        }
        try{
            bufferedWriter.close();
        } catch(IOException e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public boolean open(String fileName){
//                                                                             |
    openError = "";
    FileReader fileReader;
    try{
        fileReader = new FileReader(jarDir + "/saves/" + fileName);
    } catch(java.io.FileNotFoundException e){
        openError = "File not found";
        return false;
    }
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    String string;
    try{
        string = bufferedReader.readLine();
    } catch(IOException e){
        openError = e + "";
        return false;
    }
    output = new ArrayList<String>();
    int length = Integer.parseInt(string);
    for(int i = 0; i < length; i++){
        try{
            string = bufferedReader.readLine();
        } catch(IOException e){
            openError = e + "";
            return false;
        }
        if(string != null){
            System.out.println(string);
            output.add(string);
        }
    }
    fileRead = fileName;
    return true;
    }

    public String getOpenError(){

        return openError;
    }

    public Runner0 getRunner(){
        return runner;
    }

    public Corp0 getCorp(){
        return corp;
    }
}
