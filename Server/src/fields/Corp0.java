package fields;

import java.util.ArrayList;

public class Corp0{

    public ArrayList<String> deck;
    public boolean makingDeck;

    public Corp0(){
        makingDeck = true;
        deck = new ArrayList<String>();
    }

    public String addCard(String card){
 
        if(makingDeck){
            deck.add(card);
            return Constants.normalText + "Successfully added card";
        }
        else{
            return Constants.normalText + "You said you were done making your "
                + "deck";
        }
    }

    public void doneMakingDeck(){

        makingDeck = false;
        shuffle();
    }

    public void shuffle(){

    }
}
