import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class Actions{

  private Fields fields;
  private DataOutputStream out;
  private Scanner userInput;
  private Commands commands;
  private Constants constants;

  public Actions(DataOutputStream outIn, Fields fieldsIn){

    out = outIn;
    userInput = new Scanner(System.in);
    fields = fieldsIn;
    commands = new Commands(fields, out);
    constants = Constants.getSidedConstants(fields.getSide());
  }

  public void actions(){
    
    Modes mode = fields.getMode().getMode();
    if(mode == Modes.CHAT){
      write();
    }

    else if(mode == Modes.COMMAND){
      command();
    }

    else{
      System.out.println("Problem");
      System.out.println("There is an unrecognized mode in the class"
          + "Actions. That mode is " + mode);
    }
        
    actions();
  }
//                                                                             |
  public void write(){
//                                                                             |
    System.out.print(fields.getMode().getModeText());
    String message = userInput.nextLine();
    String firstChar = "";
    if(message.length()> 0){
      firstChar = message.substring(0, 1);
    }
    else{
      return;
    }
    if(firstChar.matches(constants.slash)){
      if(message.length() > 1){
        String secondChar = message.substring(1, 2);
        if(secondChar.matches(constants.q)){
          commands.command(0);
        }
        else if(secondChar.matches(constants.c)){
          fields.getMode().setMode(Modes.COMMAND);
        }
        else{
          System.out.println(message + " is not recognized as a"
              + "command");
          System.out.println("Sorry if it should be");
          System.out.println("Please don't start with / if you don't"
              + "want to type a command");
          System.out.println("If you want to start with /, start with"
              + "a space and then a slash");
        }
      }
    }
    else{
      try{
        out.writeUTF(constants.chat + constants.name + ": "
            + message + constants.normalText);
      } catch(IOException e){
        System.out.println("There was an IOException when trying to "
            + "write to the Server. This was in the class Actions");
        System.out.println(e);
      }
    }
  }

  public void command(){

    ArrayList<Integer> options;
    System.out.print(constants.actionPrompts);
    if(fields.getGameStarted()){
      System.out.println("(q)uit");
      System.out.println("(c)hat");
      options = new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 1}));
    }
    else{
      System.out.println("(q)uit");
      System.out.println("(c)hat");
      System.out.println("add card to your (d)eck");
      options = new ArrayList<Integer>(Arrays.asList(new Integer[] {0, 1, 2}));
    }
    System.out.print(fields.getMode().getModeText());
    String message = userInput.nextLine();
    if(message.length() > 0){
      String firstChar = message.substring(0,1);
      int index = fields.commands.indexOf(firstChar);
      if(!options.contains(index)){
        System.out.println("you cannot do that at this time");
        return;
      }
      if(index == -1){
        System.out.println(message + " did not start with any of the"
            + "avaliable commands");
      }
      else{
        commands.command(index);
      }
    }
  }
}
