import java.util.Scanner;

public class Launch{

  public static void main (String[] args){

    Players player;

    if(args.length > 0){

      if(args[0].substring(0,1).toLowerCase().matches("r")){
        player = Players.RUNNER;
      }
      else if (args[0].substring(0,1).toLowerCase().matches("c")){
        player = Players.CORP;
      }
      else{
        player = askSide();
      }
        

    }
    else{
      player = askSide();
    }
  new Connect(player);
  }
    

  private static Players askSide(){
    Scanner userInput = new Scanner(System.in);
    System.out.println("Are you the (C)orp or the (R)unner?");
    while(true){
      String answer = userInput.nextLine();
      if(answer.substring(0,1).toLowerCase().matches("r")){
        return(Players.RUNNER);
      }
      else if(answer.substring(0,1).toLowerCase().matches("c")){
        return(Players.CORP);
      } 
      else{
        System.out.println("Please type R or C");
      }
    }
  }
}
