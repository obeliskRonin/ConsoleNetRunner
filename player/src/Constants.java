public class Constants{

  public String normalText;
  public String name;
  public String slash;
  public String c;
  public String q;
  public String quit;
  public String chatMode;
  public String commandMode;
  public String chat;
  public String actions;
  public String actionPrompts;
  public String greeting;
  public String farewell;
  public int inputWaitTime;

  public Constants(){
    normalText = "[0;40;37m";
    slash = "/";
    c = "c";
    q = "q";
    quit = "/q";
    inputWaitTime = 200;

  }

  public static Constants getSidedConstants(Players player){
    if(player == Players.RUNNER){
      return(new RunnerConstants());
    }
    else{
      return(new CorpConstants());
    }
  }
}
