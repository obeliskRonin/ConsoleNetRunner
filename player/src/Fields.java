import java.util.ArrayList;
import java.util.Arrays;

public class Fields{

  private boolean leave;
  private Mode mode;
  private boolean gameStarted;
  private Players side;

  public static final ArrayList<String> commands
      = new ArrayList<String>(Arrays.asList(new String[]
      {"q", "c", "d"}));

  public Fields(Players sideIn){

    side = sideIn;
    leave = false;
    mode = new Mode(side);
    gameStarted = false;
  }
    
  private void setLeave(boolean newLeave){
    leave = newLeave;
  }
    
  public boolean getLeave(){
    return leave;
  }

  public void leave(){
    leave = true;
  }

  public Mode getMode(){
    return mode;
  }

  public boolean getGameStarted(){
    return gameStarted;
  }

  public void gameStarted(){
    gameStarted = true;
  }

  public Players getSide(){
    return side;
  }
  
  public void setSide(Players player){
    side = player;
  }
}
