public class Mode{

  private Modes mode;
  private String modeText;
  private Constants constants;

  public Mode(Players side){
    
    constants = Constants.getSidedConstants(side);
    mode = Modes.CHAT;
    modeText = constants.chatMode;
  }

  public void setMode(Modes modeIn){
   
    mode = modeIn;

    if(modeIn == Modes.CHAT){
      modeText = constants.chatMode;
    }

    else if(modeIn == Modes.COMMAND){
      modeText = constants.commandMode;
    }

    else{
      modeText = "unrecognized mode in the class Mode> ";
    }
  }

  public Modes getMode(){
    return mode;
  }

  public String getModeText(){
    return modeText;
  }
}
