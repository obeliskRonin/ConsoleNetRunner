public class CorpConstants extends Constants{

/*
    public static final String corp = "Corp";
    public static final String chatMode = (char)27 + "[35m" + corp + "|chat> "
        + (char)27 + "[32m";
    public static final String commandMode = (char)27 + "[35m" + corp
        + "|command> " + (char)27 + "[32m";
    public static final String corpChat = normalText + (char)27 + "[36m";
    public static final String corpActions = normalText + (char)27 + "[34m";
    public static final String actionPronpts = (char)27 + "[1;34m";
    public static final String greeting = corpActions + "The Corp has joined"
        + normalText;
    public static final String farewell = corpActions + "The Corp has left"
        + normalText;
*/

  public CorpConstants(){
  name = "Corp";
  chatMode = (char)27 + "[35m" + name + "|chat> " + (char)27 + "[32m";
  commandMode = (char)27 + "[35m" + name + "|command> " + (char)27 + "[32m";
  chat = normalText + (char)27 + "[36m";
  actions = normalText + (char)27 + "[34m";
  actionPrompts = (char)27 + "[1;34m";
  greeting = actions + "The Corp has joined" + normalText;
  farewell = actions + "The Corp has left" + normalText;
  }
}
