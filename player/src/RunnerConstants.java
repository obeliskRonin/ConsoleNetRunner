public class RunnerConstants extends Constants{

/*
  public static final String runner = "Runner";
  public static final String chatMode = (char)27 + "[1;35m" + runner
      + "|chat> " + (char)27 + "[33m";
  public static final String commandMode = (char)27 + "[1;35m" + runner
      + "|command> " + (char)27 + "[33m";
  public static final String runnerChat = normalText + (char)27 + "[33m";
  public static final String runnerActions = normalText + (char)27 + "[31m";
  public static final String actionPrompts = (char)27 + "[1;31m";
  public static final String greeting = runnerActions + "The Runner has "
      + "joined" + normalText;
  public static final String farewell = runnerActions + "The Runner has left"
      + normalText;
*/

  public RunnerConstants(){
    name = "Runner";
    chatMode = (char)27 + "[1;35m" + name + "|chat> " + (char)27 + "[33m";
    commandMode = "[1;35m" + name + "|command> " + (char)27 + "[33m";
    chat = normalText + (char)27 + "[33m";
    actions = normalText + (char)27 + "[31m";
    actionPrompts = (char)27 + "[1;31m";
    greeting = actions + "The Runner has joined" + normalText;
    farewell = actions + "The Runner has left" + normalText;
  }
}
