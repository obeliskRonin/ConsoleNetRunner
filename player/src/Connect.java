import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Connect{

  private Socket socket;
  private DataInputStream in;
  private DataOutputStream out;
  private Scanner userInput;
  private Input input;
  private Actions actions;
  private Fields fields;
  private boolean successful;
  private Players player; 
  private Constants constants;
	
  public Connect(Players side){

    if(side == Players.RUNNER){
      constants = new RunnerConstants();
    }
    else{
      constants = new CorpConstants();
    }


    successful = false;
  	fields = new Fields(side);
    System.out.println(constants.normalText + "                            "
        + "                                                                "
        + "                                                                "
        + "                                                                "
        + "                                                              ");
    System.out.print(constants.normalText + "\b");
    System.out.println(constants.normalText);
    userInput = new Scanner(System.in);
    System.out.println("What't the ip?");
    System.out.println("Possible options are:");
    System.out.println("localhost");
    System.out.println("192.168.5.22");
    String ip = userInput.nextLine();
                                                                            
    connect(ip);
  }

  public void connect(String ip){

    try{
      socket = new Socket(ip, 7665);
      out = new DataOutputStream(socket.getOutputStream());
      in = new DataInputStream(socket.getInputStream());
      out.writeUTF(constants.name);
    } catch(ConnectException e){
      System.out.println("ConnectException " + e);
      System.out.println("1");
      return;
    } catch(IOException e){
      System.out.println("IOException " + e);
      System.out.println("1");
    }
    try{
      Thread.sleep(10);
    } catch(InterruptedException e){
      System.out.println("Interruped Exception " + e);
      System.out.println("2");
    }
      String message;
    try{
      message = in.readUTF();
    } catch (IOException e){
      System.out.println("IOException" + e);
      System.out.println("3");
      message = "IOException" + e;
    }
    System.out.println("First normal" + (char)27 + "[30m" + "black"
        + (char)27 + "[31m" + "red" + (char)27 + "[32m" + "green" + (char)27
        + "[33m" + "yellow" + (char)27 + "[34m" + "blue" + (char)27 + "[35m"
        + "magenta" + (char)27 + "[36m" + "cyan" + (char)27 + "[37m"
        + "white");
    System.out.println("Second bright" + (char)27 + "[1m" + (char)27
        +"[30m" + "black" + (char)27 + "[31m" + "red" + (char)27 + "[32m"
        + "green" + (char)27 + "[33m" + "yellow" + (char)27 + "[34m"
        + "blue" + (char)27 + "[35m" + "magenta" + (char)27 + "[36m"
        + "cyan" + (char)27 + "[37m" + "white" + constants.normalText);
    System.out.println(message);

    Boolean success;
    try{
      success = in.readBoolean();
    } catch (IOException e){
      System.out.println("IOException " + e);
      System.out.println("4");
      success = false;
    }

    if(!success){
      System.out.print((char)27 + "[0m");
      System.exit(0);
    }

    actions = new Actions(out, fields);
    input = new Input(in, fields, out);
    Thread thread = new Thread(input);
//    try{
//      out.writeUTF(Constants.greeting);
//    } catch(Exception e){
//      System.out.println("IOException " + e);
//      System.out.println("5");
//    }
    thread.start();
    actions.actions();
    successful = true;
  }
}
