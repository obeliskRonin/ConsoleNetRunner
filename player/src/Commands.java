import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Commands{

  private Fields fields;
  private DataOutputStream out;
  private Scanner userInput;
  private Constants constants;

  public Commands(Fields fieldsIn, DataOutputStream outIn){

    fields = fieldsIn;
    out = outIn;
    userInput = new Scanner(System.in);
    constants = Constants.getSidedConstants(fields.getSide());
  }

  public void command(int number){

    if(number == 0){
      quit();
    }
    else if(number == 1){
      chat();
    }
    else if(number == 2){
      addCard();
    }
  }

  private void quit(){
    fields.leave();
    try{
      out.writeUTF(constants.farewell);
//       try{
//        Thread.sleep(1000);
//         } catch(Exception e){
//          }
      out.writeUTF(constants.quit);
    } catch(IOException e){
      System.out.println("There was an IOException when trying to"
          + " send to the Server. This was in the class Commands");
      System.out.println();
      System.out.println(e);
    }
    System.out.print("[0m");
    System.exit(0);
  }

  private void chat(){
    fields.getMode().setMode(Modes.CHAT);
  }

  private void addCard(){
    System.out.println(constants.actionPrompts + "what card do you wan't "
        + "to add? We support (P)ower for the core set, (B)rain for "
        + "creation and controll, and (W)ifi for any genisis cycle pack.");
    System.out.print(fields.getMode().getModeText());
    String card = userInput.nextLine();
    try{
      out.writeUTF("/c" + card);
    } catch(IOException e){
      System.out.println("There was an IOException when trying to "
          + " send to the Server. This is in class Commands");
    }
  }
}
